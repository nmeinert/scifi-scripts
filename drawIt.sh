#!/bin/bash

if [ "$#" -ne 2 ]; then
  echo Usage $0 [gpscript.gp] [digest]
  exit 1
fi

if [ ! -f "$1" ]; then
  echo Error: File \'$1\' does not exists.
  exit 1
fi

digest=$2
csvfile="timing_vs_avgRelDif_${digest}.csv"
imgfile="img_${digest}.png"
texfile="img_${digest}.tex"
basename="${texfile%.*}"
gpfile=".${1}.tmp"

if [ ! -f $csvfile ]; then
  echo Error: CSV file \'$csvfile\' does not exists.
  exit 1
fi

echo -e "set terminal epslatex color standalone size 4,3" > $gpfile
echo -e "set datafile separator ';'" >> $gpfile
echo -e "set key autotitle columnhead" >> $gpfile
echo -e "set output '${texfile}'\n" >> $gpfile
echo -e "datafile = '${csvfile}'\n\n" >> $gpfile
cat $1 >> $gpfile

gnuplot $gpfile

epstopdf ${basename}-inc.eps
pdflatex -interaction=nonstopmode ${texfile}
if [ "$?" -ne 0 ]; then
  echo Failure
  exit 1
fi

convert -density 600 -units PixelsPerInch ${basename}.pdf ${imgfile}
rm ${basename}{.aux,-inc.eps,-inc.pdf,.log,.pdf,.tex}
rm ${gpfile}
