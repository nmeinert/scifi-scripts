# To run this script:
# > lb-run LHCb/latest bash -f
# > python compareTimings.py

import os
import re
import sys

import ROOT


path = None
refDigest = None
keyName = 'hash_md5'
summaryFileName = 'summary_files.csv'

def printHelp():
    print('Options:')
    print('  {} [root directory] [summary file] [reference digest]'.format(sys.argv[0]))
    print('\nWhen-not-given-behavior:')
    print('  - [root directory]: you will be asked to enter the path')
    print('  - [summary file]: default value \'{}\''.format(summaryFileName))
    print('  - [reference digest]: default digest is the first entry')


def check(msg, rc):
    print('  {}: {}'.format(msg, 'OK' if rc else 'FAILURE'))
    assert rc

def getBinContents(filename):
    rootFile = ROOT.TFile.Open(os.path.join(path, filename), 'read')
    rootFile.cd()
    hist = rootFile.Get('MCFTDepositMonitor/SignalDirect/ContributionsPerPseudoChannel') 
    return [hist.GetBinContent(i) for i in range(1, hist.GetNbinsX() + 1)]


if len(sys.argv) > 1 and sys.argv[1] in ['-h', '--help']:
    printHelp()
    exit(-1)

if len(sys.argv) > 1:
    path = sys.argv[1]
else:
    path = input('Enter path to root directory: ')

if len(sys.argv) > 2:
    summaryFileName = sys.argv[2]

if len(sys.argv) == 4:
    refDigest = sys.argv[3]

print('Checking path \'{}\''.format(path))
check('does directory exist', os.path.isdir(path))
check('does \'{}\' exist'.format(summaryFileName), os.path.isfile(os.path.join(path, summaryFileName)))
    
with open(os.path.join(path, summaryFileName), 'r') as summaryFile:
    splittedLines = [line.strip().split(';') for line in summaryFile.readlines() if line.strip()]
    head, body = splittedLines[0], splittedLines[1:]
    db = [dict(zip(head, row)) for row in body]

digests = [row[keyName] for row in db]
gaussFiles = [(d, 'gauss_{}.log'.format(d)) for d in digests]
rootFiles = [(d, 'boole_{}.root'.format(d)) for d in digests]

if not refDigest:
    print('')
    header = [key for key in db[0].keys() if key != keyName]
    rowFormat = '{:>12}' * (len(header) + 1)
    print(' ' * 6 + rowFormat.format(keyName, *header))
    print(' ' * 6 + '-' * 12 * (len(header) + 1))

    i = 1
    for row in db:
        values = [row[keyName],] + [row[key] for key in header]
        values = [v[0:8] + '...' if len(v) > 11 else v for v in values]
        print('[{:>2d}]  '.format(i) + rowFormat.format(*values))
        i = i + 1
    print('Which sample do you want to use as reference? (default: 1)')
    while not refDigest:
        try:
            userInput = input('Enter number or press ENTER for default value: ')
        except SyntaxError:
            index = 0
        else:
            try:
                index = int(userInput) - 1
            except ValueError:
                index = -1
        if index >= 0 and index < len(db):
            refDigest = db[index][keyName]
            print('You chose index: {}'.format(index))
        else:
            print('Error: Invalid input! Enter number between 1 and {}'.format(len(db)))
print('\nChecking hash')
check('does hash \'{}\' exist'.format(refDigest), refDigest in digests)

for _, filename in gaussFiles + rootFiles:
    filepath = os.path.join(path, filename)
    check('does \'{}\' exist'.format(filename), os.path.isfile(filepath))

timings = {}
for digest, filename in gaussFiles:
    with open(os.path.join(path, filename), 'r') as gaussFile:
        pattern = re.compile(r'TimingAuditor.TIMER\s+INFO\s+(?P<type>(GiGaFlush|Generation))\s+\|\s+(?P<time>(\d+\.\d+))')
        matches = [pattern.search(line) for line in gaussFile.readlines() if pattern.match(line)]
        types = [m.group('type') for m in matches]
        times = [float(m.group('time')) for m in matches]
        assert len(matches) == len(times) == 2

        timing = dict(zip(types, times))
        timings[digest] = timing['GiGaFlush'] / timing['Generation']

avgRelDifs = {}
refHist = getBinContents('boole_{}.root'.format(refDigest))

print('\nProcessing')
for digest, filename in rootFiles:
    print('  {}'.format(filename))
    h = getBinContents(filename)
    assert len(refHist) == len(h)

    zipped = [(x, y) for (x, y) in zip(h, refHist) if (x != 0 and y != 0) or (x == y)]
    avgRelDifs[digest] = sum([1 if x == y == 0 else float(x) / float(y) for (x, y) in zipped]) / float(len(h))
print('completed.')

outfilename = 'timing_vs_avgRelDif_{}.csv'.format(refDigest)
print('\nWriting timinings and average of rel. differences to file \'{}\''.format(outfilename))
with open(outfilename, 'w') as outfile:
    outfile.write('digest;timging;avgRelDif\n')
    for digest in digests:
        outfile.write('{};{};{}\n'.format(digest, timings[digest], avgRelDifs[digest]))
